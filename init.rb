Redmine::Plugin.register :time_entry_change_notifier do
  name "Time Entry Change Notifier plugin"
  author "ClearCode Inc."
  description "Notify time entry change by e-mail"
  version "1.0.0"
  url "https://gitlab.com/redmine-plugin-time-entry-change-notifier/redmine-plugin-time-entry-change-notifier"
  author_url "https://gitlab.com/clear-code/"
  directory __dir__
end

prepare = lambda do
  # Load
  TimeEntryChangeNotifier
end

# We need to initialize explicitly with Redmine 5.0 or later.
prepare.call if Redmine.const_defined?(:PluginLoader)

Rails.configuration.to_prepare(&prepare)
